# SpicyKatsuLib

Fun, simple library for Vue3. Something for me to play around with in my projects.  

Feel free to clone the repo and run the `dev` script. The playground has an example of every component and directive. 

Docs: [SpicyKatsu Docs](https://satoru8.gitlab.io/spicykatsuvlib/)