import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import type { UserConfig } from 'vite'

export default defineConfig({
  plugins: [vue()],
  build: {
    lib: {
      entry: 'src/index.ts',
      name: 'SpicyKatsu',
      formats: ['es'],
      fileName: (format) => `spicykatsu.${format}.js`
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue'
        },
        inlineDynamicImports: false,
        manualChunks(id) {
        	if (id.includes('src/components')) {
        	  return id.split('src/components/')[1].split('.vue')[0]
        	}
          },
        chunkFileNames: 'lib/[name]/[name].js',
        assetFileNames: 'lib/[name]/[name].[ext]'
      }
    },
    minify: true,
    cssCodeSplit: true,
    // emptyOutDir: true,
    // sourcemap: true
  },
  optimizeDeps: {
    include: ['vue']
  },
  css: {
    modules: {
      generateScopedName: '[local]'
    }
  }
}) as UserConfig
