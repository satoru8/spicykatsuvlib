import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "SpicyVue",
  description: "Docs",
  base: '/spicykatsuvlib/',
  head: [
    ['link', { rel: 'icon', href: './assets/facicon.ico' }],
    ['meta', { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' }],
    ['meta', { name: 'author', content: 'Satoru' }],
    ['meta', { name: 'keywords', content: 'SpicyKatsu, Docs, Vue, Vite, Lib, Component, Library, Component Library' }],
  ],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Quick Start', link: '/quickstart/' },
      { text: 'Components', link: '/components/' },
      { text: 'Utilities', link: '/utilities/' }
    ],
    sidebar: {
      '/quickstart/': [
        {
          text: 'Setup',
          items: [
            
          ]
        }
      ],
      '/components/': [
        {
          text: 'Components',
          items: [
            { text: 'Main', link: '/components/' },
            { text: 'Spicy Accordion', link: '/components/spicyaccordion' },
            { text: 'Spicy Alert', link: '/components/spicyalert' },
            { text: 'Spicy Button', link: '/components/spicybtn' },
            { text: 'Spicy Carousel', link: '/components/spicycarousel' },
            { text: 'Spicy Divider', link: '/components/spicydivider' },
            { text: 'Spicy Dropdown', link: '/components/spicydropdown' },
            { text: 'Spicy Label', link: '/components/spicylabel' },
            { text: 'Spicy Modal', link: '/components/spicymodal' },
            { text: 'Spicy Progress', link: '/components/spicyprogress' },
            { text: 'Spicy Sheet', link: '/components/spicysheet' },
            { text: 'Spicy Slider', link: '/components/spicyslider' },
            { text: 'Spicy Tabs', link: '/components/spicytabs' },
            { text: 'Spicy Text Area', link: '/components/spicytextarea' },
            { text: 'Spicy Toggle', link: '/components/spicytoggle' },
            { text: 'Spicy Tooltip', link: '/components/spicytooltip' },
            { text: 'Spicy Tree', link: '/components/spicytree' }
          ],
          collapsed: false
        }
      ],
      '/utilities/': [
        {
          text: 'Utilities',
          items: [
            { text: 'Main', link: '/utilities/' }
          ]
        }
      ]
    },
    socialLinks: [
      { icon: 'github', link: 'https://github.com/satoru8' },
      { icon: 'npm', link: 'https://www.npmjs.com/package/spicykatsu' },
      { icon: 'discord', link: 'https://discord.gg/fjvwb95' },
    ],
    editLink: {
      pattern: 'https://gitlab.com/satoru8/spicykatsuvlib/edit/main/docs/:path',
      text: 'Edit this page on GitLab'
    },
    footer: {
      copyright: 'Copyright © 2024 Satoru'
    }
  }
})