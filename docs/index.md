---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: 'SpicyKatsuLib'
  # text: 'SpicyVue'
  tagline: Vue 3 component library
  actions:
    - theme: brand
      text: Quick Start
      link: quickstart/index
    - theme: alt
      text: Components
      link: components/index
    - theme: alt
      text: Utilities
      link: utilities/index

features:
  - title: Heating up
    details: SpicyKatsu offers a range of stylish components to enhance your Vue 3 projects.
  - title: Getting warm
    details: With SpicyKatsu, you can easily integrate interactive elements to engage users effectively.
  - title: Spicy!
    details: Create dynamic and visually appealing interfaces using SpicyKatsu's intuitive components.
---
