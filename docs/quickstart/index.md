# Setting up the project

This is the quickstart guide for setting up the project.

By default, components and styles are enabled.
Setting the useStyles to false will disable the css variables. Not the style.css import.

It is recommended to use the default settings.

## Prerequisites
Vue  3.x

## Installation

```bash
npm i spicykatsu
```

## Usage

```ts
import { createApp } from 'vue'
import App from './App.vue'
import { SpicyKatsu } from 'spicykatsu'
import 'spicykatsu/dist/style.css'

const app = createApp(App)
app.use(SpicyKatsu)
app.mount('#app')
```

## Settings 

```ts
app.use(SpicyKatsu, {
    useComponents: false, // Default: true
    useStyles: false      // Default: true
})