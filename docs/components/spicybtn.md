# SpicyBtn

Welcome to the documentation for the SpicyBtn component. This page documents the API for the SpicyBtn component.

## Props

| Prop          | Type          | Default    | Description                                                           |
| ------------- | ------------- | ---------- | --------------------------------------------------------------------- |
| `variant`     | String        | `'filled'` | The variant of the button. Possible values: `'filled'`, `'outlined'`. |
| `disabled`    | Boolean       | `false`    | Whether the button is disabled.                                       |
| `text`        | String        | `'Button'` | The text content of the button.                                       |
| `icon`        | String        | -          | The icon to display in the button.                                    |
| `bgColor`     | String        | -          | The background color of the button.                                   |
| `textColor`   | String        | -          | The text color of the button.                                         |
| `hoverColor`  | String        | -          | The background color of the button on hover.                          |
| `borderColor` | String        | -          | The border color of the button (only for the outlined variant).       |
| `fontSize`    | Number/String | `14`       | The font size of the button text.                                     |
| `fontWeight`  | Number/String | `500`      | The font weight of the button text.                                   |

## Slots

The SpicyBtn component supports the default slot for adding custom content inside the button.

## Events

The SpicyBtn component emits a `click` event when clicked.

## Styling

- **spicyBtn**: Style class for the button.
- **spicyBtnIcon**: Style class for the button icon.
- **spicyBtnText**: Style class for the button text.

- The button's styles can be customized through CSS variables. If you want to override the styles, use the following CSS variables in your component's `<style>` tag:

```css
--skFontSize: 14px; /* Adjust font size */
--skFontWeight: 500; /* Adjust font weight */
--skBgColor: #28292a; /* Adjust background color */
--skTextColor: #ddd; /* Adjust text color */
--skBorderColor: #515353; /* Adjust border color for 'outlined' variant */
--hoverColor: grey; /* Adjust background color on hover */
```

## Example

```vue
<template>
  <div>
    <SpicyBtn text="Primary" />
    <SpicyBtn variant="outlined" text="Outlined" />
    <SpicyBtn text="Disabled" disabled />
    <SpicyBtn text="With Icon" icon="🚀" />
  </div>
</template>

<script setup>
import { SpicyBtn } from 'spicykatsu'
</script>
```
