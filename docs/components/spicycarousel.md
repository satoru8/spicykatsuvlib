# Carousel

Welcome to the documentation for the carousel component. This page documents the API for the carousel component.

## Props

| Prop            | Type     | Default | Description                                      |
|-----------------|----------|---------|--------------------------------------------------|
| `autoplay`      | Boolean  | `false` | Enables automatic playback of the carousel slides. |
| `autoplaySpeed` | Number   | `5000`  | The interval (in milliseconds) between autoplay transitions. |
| `showNavigation`| Boolean  | `true`  | Whether to show navigation controls (previous and next buttons). |
| `showPagination`| Boolean  | `true`  | Whether to show pagination controls (page buttons). |
| `activeColor`   | String   | `'rgba(0, 255, 234, 0.5)'` | The color of the active pagination button. |
| `loop`          | Boolean  | `true`  | Whether the carousel should loop back to the beginning after reaching the last slide. |
| `initialSlide`  | Number   | `0`     | The index of the initial slide to display. |
| `slides`        | Array    | -       | An array of slide objects. Each slide object should have `img` (URL of the slide image) and `text` (optional text overlay) properties. |
| `width`         | String   | `'540px'` | The width of the carousel container. |
| `height`        | String   | `'300px'` | The height of the carousel container. |
| `fullScreen`    | Boolean  | `false` | Whether to display the carousel in full-screen mode. |
| `enableImageClick` | Boolean | `false` | Whether clicking on the carousel image should open the image in a new tab. |

## Methods

| Method        | Description                            |
|---------------|----------------------------------------|
| `prevSlide()` | Moves to the previous slide.           |
| `nextSlide()` | Moves to the next slide.               |
| `goToSlide(index: number)` | Navigates to the slide at the specified index. |

## Events

| Event           | Payload                        | Description                                          |
|-----------------|--------------------------------|------------------------------------------------------|
| `slideChange`   | `{ newIndex: number }`         | Emitted when the active slide changes.               |

## Example

```vue
<template>
  <div class="carousel-container">
    <SpicyCarousel
      :autoplay="true"
      :autoplaySpeed="5000"
      :showNavigation="true"
      :showPagination="true"
      :activeColor="'rgba(0, 255, 234, 0.5)'"
      :loop="true"
      :initialSlide="0"
      :slides="slides"
      width="540px"
      height="300px"
      :fullScreen="false"
      :enableImageClick="true"
    />
  </div>
</template>

<script setup>
const slides = [
  { img: 'https://example.com/slide1.jpg', text: 'Slide 1' },
  { img: 'https://example.com/slide2.jpg', text: 'Slide 2' },
  { img: 'https://example.com/slide3.jpg', text: 'Slide 3' }
]
</script>
