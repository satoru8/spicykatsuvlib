# SpicyModal

Welcome to the documentation for the SpicyModal component. This page documents the API for the SpicyModal component.

## Props

| Prop           | Type    | Default     | Description                                                               |
| -------------- | ------- | ----------- | ------------------------------------------------------------------------- |
| `width`        | String  | `'50%'`     | The width of the modal dialog.                                            |
| `height`       | String  | `'auto'`    | The height of the modal dialog.                                           |
| `borderRadius` | String  | `'8px'`     | The border radius of the modal dialog.                                    |
| `bgColor`      | String  | `'#2b2b2b'` | The background color of the modal dialog.                                 |
| `visible`      | Boolean | `false`     | Controls the visibility of the modal dialog.                              |
| `closeBtn`     | Boolean | `false`     | Determines whether to display a close button in the modal dialog header.  |
| `actions`      | Array   | `[]`        | An array of objects representing additional action buttons for the modal. |
| `closeOnClick` | Boolean | `true`      | Determines whether clicking outside the modal dialog closes it.           |
| `modalTitle`   | String  | `''`        | The title of the modal dialog displayed in the header.                    |

## Slots

These slots are optional.

| Slot                | Description                                                                    |
| ------------------- | ------------------------------------------------------------------------------ |
| `default`           | Main content slot for the modal body. Exposes `title` as a slot prop.          |
| `spicyModalHeader`  | Slot for custom content in the modal's header. Exposes `title` as a slot prop. |
| `spicyModalActions` | Slot for adding custom action buttons in the modal's footer.                   |

## Events

The SpicyModal component emits an `update:visible` event when the visibility of the modal changes. This event is emitted with a boolean value indicating the new visibility state.

## Styling

- **spicyModalOverlay**: Style class for the modal overlay.
- **spicyModal**: Style class for the modal box.
- **spicyModalHeader**: Style class for the modal header.
- **spicyModalActions**: Style class for the modal actions container.
- **actionBtn**: Style class for action buttons within the modal.

## Example

### No slots:

```vue
<template>
  <div>
    <button @click="openModal">Open Modal</button>
    <SpicyModal v-model:visible="modalVisible" :actions="modalActions" modalTitle="Example Modal">
      <p>This is the content of the modal.</p>
    </SpicyModal>
  </div>
</template>

<script setup>
import { ref } from 'vue'
import { SpicyModal } from 'spicykatsu'

const modalVisible = ref(false)
const modalActions = [
  { label: 'Action 1', handler: action1Handler },
  { label: 'Action 2', handler: action2Handler },
]

function openModal() {
  modalVisible.value = true
}

function action1Handler() {
  console.log('Action 1 clicked')
}

function action2Handler() {
  console.log('Action 2 clicked')
}
</script>
```

### With slots:

```vue
<template>
  <SpicyModal :visible="isModalOpen" @update:visible="handleModalVisibility">
    <template #spicyModalHeader>
      <h2>Custom Header</h2>
    </template>
    Some content here
    <template #spicyModalActions>
      <button @click="doSomething">Action 1</button>
    </template>
  </SpicyModal>
</template>

<script setup>
import { ref } from 'vue'
const isModalOpen = ref(false)

function handleModalVisibility(visible) {
  isModalOpen.value = visible
}

function doSomething() {
  console.log('Action 1 clicked')
}
</script>
```
