# SpicyToggle

Welcome to the documentation for the SpicyToggle component. This page documents the API for the SpicyToggle component.

## Props

| Prop          | Type    | Default    | Description                                                        |
| ------------- | ------- | ---------- | ------------------------------------------------------------------ |
| `modelValue`  | Boolean | `false`    | The current state of the toggle.                                   |
| `label`       | String  | -          | A label for the toggle, used for accessibility.                    |
| `variant`     | String  | `'filled'` | The variant of the toggle, either `'filled'` or `'outlined'`.      |
| `borderColor` | String  | -          | The color of the toggle border, if using the `'outlined'` variant. |
| `activeColor` | String  | -          | The color of the toggle when active.                               |
| `focusShadow` | String  | -          | The box shadow color when the toggle is focused.                   |

## Events

The SpicyToggle component emits an `update:modelValue` event when the state of the toggle changes. This event is emitted with a boolean value indicating the new state of the toggle.

## Example

```vue
<template>
  <div>
    <label for="toggle">Enable Feature</label>
    <SpicyToggle v-model:modelValue="toggleState" label="Enable Feature" />
  </div>
</template>

<script setup>
import { ref } from 'vue'
import { SpicyToggle } from 'spicykatsu'

const toggleState = ref(false)
</script>
```
