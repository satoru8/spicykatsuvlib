# SpicyDivider

Welcome to the documentation for the SpicyDivider component. This page documents the API for the SpicyDivider component.

## Props

| Prop      | Type   | Default              | Description                                                               |
| --------- | ------ | -------------------- | ------------------------------------------------------------------------- |
| `variant` | String | `'solid'`            | The variant of the divider. Possible values: `'solid'`, `'dashed'`.       |
| `width`   | String | `'100%'`             | The width of the divider. Can be any valid CSS width value.               |
| `height`  | String | `'1px'`              | The height (thickness) of the divider. Can be any valid CSS height value. |
| `bgColor` | String | `'var(--skBgColor)'` | The color of the divider. Can be any valid CSS color value.               |

## Slots

The SpicyDivider component supports the default slot for adding custom content, which will be displayed above the divider line.

## Methods

The SpicyDivider component does not expose any methods.

## Events

The SpicyDivider component does not emit any events.

## Styling

- **spicyDivider**: Base style class for the divider.

## Example

```vue
<template>
  <div>
    <SpicyDivider variant="solid" width="50%" height="2px" bgColor="#FF5733">
      <span>Solid Divider</span>
    </SpicyDivider>

    <SpicyDivider variant="dashed" width="75%" height="3px" bgColor="#3498DB" />

    <SpicyDivider bgColor="#2ECC71">
      <span>Default Solid Divider</span>
    </SpicyDivider>
  </div>
</template>
```
