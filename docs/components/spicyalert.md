# SpicyAlert

Welcome to the documentation for the SpicyAlert component. This page documents the API for the SpicyAlert component.

## Props

| Prop        | Type    | Default  | Description                                                                                                        |
| ----------- | ------- | -------- | ------------------------------------------------------------------------------------------------------------------ |
| `variant`   | String  | `'info'` | The variant of the alert. Possible values: `'info'`, `'success'`, `'warning'`, `'error'`, `'custom'`.              |
| `text`      | String  | -        | The text content of the alert.                                                                                     |
| `icon`      | String  | -        | The icon to display in the alert.                                                                                  |
| `iconOnly`  | Boolean | `false`  | Whether the alert should display the icon only.                                                                    |
| `closable`  | Boolean | `false`  | Whether the alert should have a close button.                                                                      |
| `autoClose` | Number  | `0`      | The duration (in milliseconds) after which the alert should automatically close. Set to `0` to disable auto-close. |

## Slots

The SpicyAlert component supports the default slot for adding custom content inside the alert.

## Methods

The SpicyAlert component does not expose any methods.

## Events

The SpicyAlert component does not emit any events.

## Styling

- **spicyAlert**: Style class for the alert.
- **spicyAlertIcon**: Style class for the alert icon.
- **spicyAlertText**: Style class for the alert text.
- **spicyAlertCloseBtn**: Style class for the close button.

## Example

```vue
<template>
  <div>
    <SpicyAlert variant="success" text="Success alert" :closable="true" />
    <SpicyAlert variant="warning" text="Warning alert" icon="🚨" />
    <SpicyAlert variant="error" text="Error alert" icon="❌" />
    <SpicyAlert variant="info" text="Info alert" :autoClose="5000" />
  </div>
</template>
```
