# SpicySheet

Welcome to the documentation for the SpicySheet component. This page documents the API for the SpicySheet component.

## Props

| Prop        | Type    | Default | Description                                          |
| ----------- | ------- | ------- | ---------------------------------------------------- |
| `isRound` | Boolean | `false` | Determines if the sheet should have rounded corners. |
| `color`     | String  | `''`    | The background color of the sheet.                   |

## Slots

The SpicySheet component supports the default slot for adding custom content inside the sheet.

## Styling

- **spicySheet**: Style class for the sheet container.
- **spicySheet.isRound**: Style class for rounded corners.

- The sheet's styles can be customized through CSS variables. If you want to override the styles, use the following CSS variables in your component's `<style>` tag:

```css
--color: transparent; /* Adjust background color */
```

```vue
<template>
  <div>
    <SpicySheet color="#f0f0f0" />
    <SpicySheet isRound color="#e0e0e0" />
  </div>
</template>

<script setup lang="ts">
import { SpicySheet } from 'spicykatsu'
</script>
