# Tooltip

Welcome to the documentation for the Tooltip component. This page documents the API for the Tooltip component.

## Props

| Prop          | Type   | Default | Description                                                                                                          |
| ------------- | ------ | ------- | -------------------------------------------------------------------------------------------------------------------- |
| `text`        | String | -       | The text content to display in the tooltip.                                                                          |
| `prependIcon` | String | -       | The icon to prepend to the text content.                                                                             |
| `appendIcon`  | String | -       | The icon to append to the text content.                                                                              |
| `position`    | String | `'top'` | The position of the tooltip relative to its host element. Possible values: `'top'`, `'bottom'`, `'left'`, `'right'`. |

## Events

None

## Example

```vue
<template>
  <div>
    <SpicyTooltip text="This is a tooltip" position="right">
      <SpicyToggle v-model="toggleState" />
    </SpicyTooltip>
  </div>
</template>
```
