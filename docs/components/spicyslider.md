# SpicySlider

Welcome to the documentation for the SpicySlider component. This page documents the API for the SpicySlider component.

## Props

| Prop         | Type   | Default | Description                                                                    |
| ------------ | ------ | ------- | ------------------------------------------------------------------------------ |
| `min`        | Number | `0`     | The minimum value of the slider.                                               |
| `max`        | Number | `100`   | The maximum value of the slider.                                               |
| `step`       | Number | `1`     | The step value for the slider.                                                 |
| `label`      | String | `''`    | The label displayed above the slider.                                          |
| `modelValue` | Number | `0`     | The current value of the slider. Used with `v-model` to bind the slider value. |

## Slots

The SpicySlider component supports the default slot for adding custom content inside the slider's container.

## Methods

The SpicySlider component does not expose any methods.

## Events

| Event               | Description                            |
| ------------------- | -------------------------------------- |
| `update:modelValue` | Emitted when the slider value changes. |

## Styling

- **spicySlider**: Base style class for the slider container.
- **spicySliderLabel**: Style class for the slider label.
- **spicySliderInput**: Style class for the slider input.

## Example

```vue
<template>
  <div>
    <SpicySlider min="0" max="100" step="5" label="Volume" v-model="volume" />

    <SpicySlider min="0" max="50" step="1" label="Brightness" v-model="brightness">
      <div>Current Brightness: {{ brightness }}</div>
    </SpicySlider>
  </div>
</template>

<script setup>
import { ref } from 'vue'
import SpicySlider from './components/SpicySlider.vue'

const volume = ref(20)
const brightness = ref(30)
</script>
```
