# SpicyInput

Welcome to the documentation for the SpicyInput component. This page documents the API for the SpicyInput component.

## Props

| Prop          | Type   | Default    | Description                                                          |
| ------------- | ------ | ---------- | -------------------------------------------------------------------- |
| `value`       | String | `''`       | The value of the input.                                              |
| `placeholder` | String | `''`       | The placeholder text for the input.                                  |
| `label`       | String | `''`       | The label text for the input.                                        |
| `error`       | String | `''`       | The error message to display for the input.                          |
| `variant`     | String | `'filled'` | The variant of the input. Possible values: `'filled'`, `'outlined'`. |

## Slots

The SpicyInput component supports the default slot for adding custom content inside the input.

## Events

The SpicyInput component does not emit any events.

## Example

```vue
<template>
  <div>
    <SpicyInput label="Username" placeholder="Enter your username" v-model="username" />
    <SpicyInput
      label="Password"
      placeholder="Enter your password"
      v-model="password"
      variant="outlined"
    />
    <SpicyInput label="Email" placeholder="Enter your email" v-model="email" :error="emailError" />
  </div>
</template>

<script setup>
import { SpicyInput } from 'spicykatsu'
import { ref, computed } from 'vue'

const username = ref('')
const password = ref('')
const email = ref('')

const emailError = computed(() => {
  if (!email.value) return 'Email is required'
  if (!isValidEmail(email.value)) return 'Invalid email address'
  return ''
})

function isValidEmail(email) {
  // Your email validation logic here
  return /\S+@\S+\.\S+/.test(email)
}
</script>
```
