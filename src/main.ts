import { createApp } from 'vue'
import { SpicyKatsu } from './index'
import Home from './playground/Playground.vue'

const app = createApp(Home)

app.use(SpicyKatsu, {
  useStyles: true,
  useDirectives: true,
  useComponents: true
})

app.mount('#playground')
