// Components
import SpicyAccordion from './components/SpicyAccordion.vue'
import SpicyAlert from './components/SpicyAlert.vue'
import SpicyBtn from './components/SpicyBtn.vue'
import SpicyCarousel from './components/SpicyCarousel.vue'
import SpicyDialog from './components/SpicyDialog.vue'
import SpicyDivider from './components/SpicyDivider.vue'
import SpicyDropdown from './components/SpicyDropdown.vue'
import SpicyFileInput from './components/SpicyFileInput.vue'
import SpicyModal from './components/SpicyModal.vue'
import SpicyProgress from './components/SpicyProgress.vue'
import SpicySheet from './components/SpicySheet.vue'
import SpicySlider from './components/SpicySlider.vue'
import SpicyTabs from './components/SpicyTabs.vue'
import SpicyTextArea from './components/SpicyTextArea.vue'
import SpicyTextField from './components/SpicyTextField.vue'
import SpicyToggle from './components/SpicyToggle.vue'
import SpicyTooltip from './components/SpicyTooltip.vue'
import SpicyTree from './components/SpicyTree.vue'
import SpicyTreeNode from './components/SpicyTreeNode.vue'

const components = {
  SpicyAccordion,
  SpicyAlert,
  SpicyBtn,
  SpicyCarousel,
  SpicyDialog,
  SpicyDivider,
  SpicyDropdown,
  SpicyFileInput,
  SpicyModal,
  SpicyProgress,
  SpicySheet,
  SpicySlider,
  SpicyTabs,
  SpicyTextArea,
  SpicyTextField,
  SpicyToggle,
  SpicyTooltip,
  SpicyTree,
  SpicyTreeNode
}

export {
  SpicyAccordion,
  SpicyAlert,
  SpicyBtn,
  SpicyCarousel,
  SpicyDialog,
  SpicyDivider,
  SpicyDropdown,
  SpicyFileInput,
  SpicyModal,
  SpicyProgress,
  SpicySheet,
  SpicySlider,
  SpicyTabs,
  SpicyTextArea,
  SpicyTextField,
  SpicyToggle,
  SpicyTooltip,
  SpicyTree,
  SpicyTreeNode
}

// Styles


// Directives
import { spicyDrag } from './directives/spicyDrag'
import { spicyRipple } from './directives/spicyRipple'

const directives = {
  spicyDrag,
  spicyRipple
}

export { spicyDrag, spicyRipple }

// Install
import { App } from 'vue'

interface SpicyLibraryOptions {
  useComponents?: boolean
  useStyles?: boolean
  useDirectives?: boolean
}

const SpicyKatsu = {
  install(app: App, options: SpicyLibraryOptions = {}) {
    const { useComponents = true, useStyles = true, useDirectives = true } = options

    if (useComponents) {
      Object.entries(components).forEach(([name, component]) => {
        app.component(name, component)
      })
    }
    // else {
    //   Object.keys(components).forEach((name) => {
    //     app.component(name, () => import(`./components/${name}.vue`))
    //   })
    // }

    if (useStyles) {
      import('./styles/main.css?inline').then((style) => {
        const styleElement = document.createElement('style' as HTMLStyleElement['tagName'])
        styleElement.id = 'SpicyStyles'
        styleElement.textContent = style.default
        document.head.appendChild(styleElement)
      })
    }

    if (useDirectives) {
      Object.entries(directives).forEach(([name, directive]) => {
        app.directive(name, directive)
      })

      import('./styles/ripple.css?inline').then((style) => {
        const styleElement = document.createElement('style' as HTMLStyleElement['tagName'])
        styleElement.id = 'SpicyRippleStyles'
        styleElement.textContent = style.default
        document.head.appendChild(styleElement)
      })
    }
  }
}

export { SpicyKatsu }
