import type { Directive } from 'vue'

interface DragOptions {
  axis?: 'x' | 'y'
}

interface HTMLElementWithCleanup extends HTMLElement {
  _cleanup?: () => void
}

export const spicyDrag: Directive<HTMLElement, DragOptions> = {
  mounted(el, binding) {
    const { axis } = binding.value || {}

    el.style.position = 'relative'
    el.style.cursor = 'move'

    let shiftX: number, shiftY: number

    const onMove = (pageX: number, pageY: number) => {
      const rect = el.getBoundingClientRect()
      let newX = pageX - shiftX
      let newY = pageY - shiftY

      if (axis !== 'y') {
        el.style.left = `${newX - rect.left + parseFloat(getComputedStyle(el).left || '0')}px`
      }
      if (axis !== 'x') {
        el.style.top = `${newY - rect.top + parseFloat(getComputedStyle(el).top || '0')}px`
      }
    }

    const onMouseMove = (event: MouseEvent) => {
      onMove(event.pageX, event.pageY)
    }

    let isDragging = false

    const onTouchMove = (event: TouchEvent) => {
      const touch = event.touches[0]
      if (touch) {
        isDragging = true
        onMove(touch.pageX, touch.pageY)
        event.preventDefault()
      }
    }

    const stopDrag = () => {
      document.removeEventListener('mousemove', onMouseMove)
      document.removeEventListener('mouseup', stopDrag)
      document.removeEventListener('touchmove', onTouchMove)
      document.removeEventListener('touchend', stopDrag)
      if (!isDragging) {
        return
      }

      isDragging = false
    }

    const startMouseDrag = (e: MouseEvent) => {
      const rect = el.getBoundingClientRect()
      shiftX = e.pageX - rect.left
      shiftY = e.pageY - rect.top

      document.addEventListener('mousemove', onMouseMove)
      document.addEventListener('mouseup', stopDrag)
    }

    const startTouchDrag = (e: TouchEvent) => {
      if (e.touches.length !== 1) return

      const touch = e.touches[0]
      if (!touch) return

      const rect = el.getBoundingClientRect()
      shiftX = touch.pageX - rect.left
      shiftY = touch.pageY - rect.top

      isDragging = false

      document.addEventListener('touchmove', onTouchMove)
      document.addEventListener('touchend', stopDrag)

      e.preventDefault()
    }

    el.addEventListener('mousedown', startMouseDrag)
    el.addEventListener('touchstart', startTouchDrag)

    const elWithCleanup = el as HTMLElementWithCleanup
    elWithCleanup._cleanup = () => {
      elWithCleanup.removeEventListener('mousedown', startMouseDrag)
      elWithCleanup.removeEventListener('touchstart', startTouchDrag)
      document.removeEventListener('mousemove', onMouseMove)
      document.removeEventListener('mouseup', stopDrag)
      document.removeEventListener('touchmove', onTouchMove)
      document.removeEventListener('touchend', stopDrag)
    }
  },
  unmounted(el) {
    const elWithCleanup = el as HTMLElementWithCleanup
    if (elWithCleanup._cleanup) {
      elWithCleanup._cleanup()
    }
  }
}
