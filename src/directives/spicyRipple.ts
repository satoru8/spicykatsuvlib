import type { Directive } from 'vue'

export const spicyRipple: Directive<HTMLElement> = {
  beforeMount(el: HTMLElement) {
    el.style.position = 'relative'
    el.style.overflow = 'hidden'

    const createRipple = (x: number, y: number) => {
      const circle = document.createElement('span')
      const diameter = Math.max(el.clientWidth, el.clientHeight)
      const radius = diameter / 2

      circle.style.width = circle.style.height = `${diameter}px`
      circle.style.left = `${x - el.getBoundingClientRect().left - radius}px`
      circle.style.top = `${y - el.getBoundingClientRect().top - radius}px`
      circle.classList.add('spicyRipple')

      const existingRipple = el.querySelector('.spicyRipple') as HTMLElement | null
      if (existingRipple) {
        existingRipple.remove()
      }

      el.appendChild(circle)

      circle.addEventListener('animationend', () => {
        circle.remove()
      })
    }

    const handleClick = (event: MouseEvent | TouchEvent) => {
      let x = 0,
        y = 0

      if (event instanceof MouseEvent) {
        x = event.clientX
        y = event.clientY
      } else if (event instanceof TouchEvent) {
        const touch = event.touches[0]
        x = touch.clientX
        y = touch.clientY
      }

      createRipple(x, y)
    }

    const hasNoRipple = el.getAttribute('data-no-ripple') !== null

    if (!hasNoRipple) {
      el.addEventListener('click', (event: MouseEvent) => {
        requestAnimationFrame(() => handleClick(event))
      })

      el.addEventListener('touchstart', (event: TouchEvent) => {
        requestAnimationFrame(() => handleClick(event))
      })
    }
  }
}
